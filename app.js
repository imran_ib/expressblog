var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var methodOverride = require('method-override');


//========================================================
mongoose.Promise = global.Promise;
//setup mongoose database
mongoose.connect("mongodb://localhost/blog__app");
var blogSchema = new mongoose.Schema({
    title:  String,
    image: String,
    body:   String,
    created : {type : Date , default : Date.now}
  });

 var Blog = mongoose.model('Blog', blogSchema);
 // Blog.create ({
 //   title : "Sexy Eyes",
 //   image : "https://images.unsplash.com/photo-1460904577954-8fadb262612c?ixlib=rb-0.3.5&s=5bcb5490cd0e0bc4e23907eea361e98b&auto=format&fit=crop&w=940&q=80",
 //   body : "beautifull and  girl Eyes"
 // });


//========================================================

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(methodOverride("_method"));


//===============================================
//   Index Route
//===============================================
app.get("/" , (req , res) =>{
  res.redirect("/blogs")
});

//===============================================
//   Index Route
//===============================================
app.get("/blogs" , (req , res )=> {
 
  Blog.find({} , (err , blogs) => {
      if(err){
        console.log(err)
      } else {
        res.render("index", {blogs : blogs})
      }
  });
});

//===============================================
//   Create New Blog 
//===============================================
  app.get("/blogs/new" , (req,res)=> {
    res.render("new")
  });

//===============================================
//   Post Route for new blog
//===============================================
app.post("/blogs" , (req , res) => {
  Blog.create(req.body.blog , (err , newBlog) => {
    if(err){throw err} 
    res.redirect("/blogs")
  });
});

//===============================================
//   Show Route
//===============================================
app.get("/blogs/:id" , (req , res) => {
  Blog.findById(req.params.id , (err , blog)=>{
    if(err) {throw err}
    else{
      res.render("show" , {blog: blog})
    }
  });
});

//===============================================
//   Edit Route
//===============================================
app.get("/blogs/:id/edit" , (req,res)=> {
  Blog.findById(req.params.id , (err , blog)=>{
    if(err) {throw err}
    else{
      res.render("edit" , {blog: blog})
    }
  });
})

//===============================================
//   PUT Route for edit 
//===============================================
app.put("/blogs/:id" , (req , res)=>{
  Blog.findByIdAndUpdate(req.params.id, req.body.blog ,(err )=>{
    if(err) {throw err}
      else {
        res.redirect("/blogs/" + req.params.id)
      }
  });
})

//===============================================
//   Remove Route1
//===============================================
app.delete("/blogs/:id" , (req , res) => {
  Blog.findByIdAndRemove(req.params.id , (err) =>{
      if(err){throw err}
        else {
          res.redirect("/blogs")
        }
  });
});

// sever 
app.listen(3000, function() {
    console.log("Server is Running on port 3000")
});

module.exports = app;